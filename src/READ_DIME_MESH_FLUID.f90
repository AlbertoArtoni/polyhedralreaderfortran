!    Copyright (C) 2012 The SPEED FOUNDATION
!    Author: Ilario Mazzieri
!
!    This file is part of SPEED.
!
!    SPEED is free software; you can redistribute it and/or modify it
!    under the terms of the GNU Affero General Public License as
!    published by the Free Software Foundation, either version 3 of the
!    License, or (at your option) any later version.
!
!    SPEED is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Affero General Public License for more details.
!
!    You should have received a copy of the GNU Affero General Public License
!    along with SPEED.  If not, see <http://www.gnu.org/licenses/>.

!> @brief Reads dimensions in gridfile (*.mesh)
!! @author Ilario Mazzieri
!> @date September, 2013
!> @version 1.0
!> @param[in] gridfile  file name (*.mesh)
!> @param[out] nb_node number of mesh node
!> @param[out] nb_hexa number of hexahedral elements
!> @param[out] nb_quad number of quadrilateral elements



      subroutine READ_DIME_MESH_FLUID(&
                           aeroPath,&
                           nb_quad_fluid, nb_hexa_fluid, nb_nodes_fluid, &
                           nb_internal_fluid, nelem_loc_fluid)

      implicit none


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE HEAD
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      character*90  :: aeroPath
      character*150 :: gridfile
      character*150 :: inline


      integer*4 :: jj,kk
      integer*4 :: nb_quad_fluid, nb_hexa_fluid, nb_nodes_fluid, &
                   nb_internal_fluid, nelem_loc_fluid

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE BODY
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      nb_nodes_fluid = 0;  nb_hexa_fluid = 0;  nb_quad_fluid = 0
      
      
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/points'))

      !> Open mesh file
      open(40,file=gridfile)

      do jj=1,18
          read(40,'(A)') inline
      enddo
       
      read(40,'(A)') inline
      read(inline,*) nb_nodes_fluid

      close(40)

      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/owner'))
      open(40,file=gridfile)

      do jj=1,12
          read(40,'(A)') inline
      enddo
       
      read(40,'(A)') inline
      jj = 1;
      do while (inline(jj:jj+6) .ne. 'nCells:')
        jj = jj+1;
      end do

      kk = jj+7;

      do while (inline(jj:jj+1) .ne. ' ')
        jj = jj+1;
      end do

      read(inline(kk:jj),*) nb_hexa_fluid

      close(40)

      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/faces'))
      open(40,file=gridfile)

      do jj=1,18
          read(40,'(A)') inline
      enddo
       
      read(40,'(A)') inline
      read(inline,*) nb_quad_fluid

      close(40)


      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/neighbour'))
      write(*,*) gridfile
      open(40,file=gridfile)

      do jj=1,19
          read(40,'(A)') inline
      enddo
      read(40,'(A)') inline
      read(inline,*) nb_internal_fluid

      close(40)

      ! this will change when parallel
      nelem_loc_fluid = nb_hexa_fluid


      write(*,*) "Damping mesh information...."
      write(*,*) "vertex number:", nb_nodes_fluid
      write(*,*) "face number:", nb_quad_fluid
      write(*,*) "cell number:", nb_hexa_fluid


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      return

      end subroutine READ_DIME_MESH_FLUID
