      subroutine READ_DIME_MESH_FLUID_ELEMENTS(&
                           aeroPath, nb_face_fluid, nb_hexa_fluid, elemFaces,&
                           nb_elems_conn_fluid)

      implicit none


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE HEAD
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      character*150 :: gridfile, inline
      character*90 :: aeroPath


      integer*4 :: jj,kk, nb_face_fluid, nb_hexa_fluid, nb_elems_conn_fluid

      integer*4, dimension(nb_hexa_fluid) :: elemFaces

      integer*4       :: ierror, intVal, val, neigVal

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE BODY
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      elemFaces = 0
      nb_elems_conn_fluid = 0
  
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/owner'))
      open(40,file=gridfile)

      do jj=1,21
          read(40,'(A)') inline
      enddo

      ! read the file
      ! access the element and sum one to the face dimensions

      do jj=1,nb_face_fluid

          read(40,'(A)') inline
          read(inline,*) intVal
          intVal = intVal + 1       ! from C++ to Fortran
          elemFaces(intVal) = elemFaces(intVal)+1  
      end do

      close(40)

      ! now read the neighbour elements
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/neighbour'))
      open(40,file=gridfile)

      do jj=1,19
          read(40,'(A)') inline
      enddo

      read(40,'(A)') inline
      read(inline,*) neigVal

      read(40,'(A)') inline     ! skip parenthesis

      ! read the file
      ! access the element and sum one to the face dimensions

      do jj=1,neigVal

          read(40,'(A)') inline
          read(inline,*) intVal
          intVal = intVal + 1       ! from C++ to Fortran
          elemFaces(intVal) = elemFaces(intVal)+1  

      end do

      close(40)

      nb_elems_conn_fluid = nb_hexa_fluid + nb_hexa_fluid;
      do jj=1,nb_hexa_fluid
          nb_elems_conn_fluid = elemFaces(jj) + nb_elems_conn_fluid;
      end do

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      return

      end subroutine READ_DIME_MESH_FLUID_ELEMENTS
