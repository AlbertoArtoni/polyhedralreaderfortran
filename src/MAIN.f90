program MAIN
    implicit none
    character*90 :: aeroPath

    integer*4 :: nb_face_fluid, nb_hexa_fluid, nb_nodes_fluid,&
                 nb_internal_fluid, nelem_loc_fluid, nb_faces_conn_fluid, &
                 nF, jj, nb_elems_conn_fluid

    integer*4, dimension(:), allocatable :: faceFluidVector, faceConnFluid, elemFaceDime, elemFaceConn
    real*8, dimension(:,:), allocatable :: points_fluid


    ! this program reads a polyhedral mesh from OpenFOAM files

    !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!!!! MODIFY HERE TO CHANGE TEST
    !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    aeroPath="../Input/gridPoly/"
    !aeroPath="../Input/gridHexa/"

    call READ_DIME_MESH_FLUID(aeroPath,&
                              nb_face_fluid, nb_hexa_fluid, nb_nodes_fluid, &
                              nb_internal_fluid, nelem_loc_fluid)

    allocate(faceFluidVector(nb_face_fluid))
    call READ_DIME_MESH_FLUID_FACES(aeroPath, nb_face_fluid, faceFluidVector, nb_faces_conn_fluid)

    write(*,*) "building conn faces..."

    allocate(faceConnFluid(0:nb_faces_conn_fluid))
    call READ_DIME_MESH_FLUID_CONN_FACES(aeroPath, nb_face_fluid, faceFluidVector, &
                                         nb_faces_conn_fluid, faceConnFluid)


    ! creo un vettore con le dimensoni delle facce per elemento
    allocate(elemFaceDime(nb_hexa_fluid))
    call READ_DIME_MESH_FLUID_ELEMENTS(&
                           aeroPath, nb_face_fluid, nb_hexa_fluid, elemFaceDime, nb_elems_conn_fluid)


    write(*,*) "reading dimension of face connectivity..."

    allocate(elemFaceConn(0:nb_elems_conn_fluid))
    call READ_MESH_FLUID_CONN_ELEMENTS(&
                           aeroPath, nb_face_fluid, nb_hexa_fluid, &
                           elemFaceDime, elemFaceConn, nb_elems_conn_fluid)

    allocate(points_fluid(nb_nodes_fluid,3))
    call READ_MESH_FLUID_POINTS(aeroPath, nb_nodes_fluid, points_fluid)


    ! creo tutti i poliedri
    ! nuova implementazione dei poliedri
    !  @todo: create polyehdral elements

    ! merge into polyhedron connectivity
    call EXPORT_VTK_POLY( nb_nodes_fluid, nb_faces_conn_fluid , nb_elems_conn_fluid, &
                          points_fluid, elemFaceConn, faceConnFluid,    &
                      "../Output/out.vtk")


    deallocate(faceFluidVector, faceConnFluid, elemFaceDime)
    deallocate(points_fluid)

end program MAIN
