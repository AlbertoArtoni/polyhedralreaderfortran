      subroutine READ_MESH_FLUID_CONN_ELEMENTS(&
                           aeroPath, nb_face_fluid, nb_hexa_fluid, &
                           elemFaces, elemFaceConn, nb_elems_conn_fluid)

      implicit none


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE HEAD
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      character*150 :: gridfile, inline
      character*90 :: aeroPath


      integer*4 :: jj,kk, nb_face_fluid, nb_hexa_fluid, nb_elems_conn_fluid

      integer*4, dimension(nb_hexa_fluid) :: elemFaces                  ! face conn
      integer*4, dimension(0:nb_elems_conn_fluid) :: elemFaceConn       ! connectivity elems
      integer*4, dimension(:), allocatable :: elemIter          ! saves the current shift

      integer*4       :: ierror, intVal, val, neigVal

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE BODY
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      elemFaceConn(0) = nb_hexa_fluid
      elemFaceConn(1) = nb_hexa_fluid + 1
      allocate(elemIter(nb_hexa_fluid))
      elemIter = 0

      do jj=2,nb_hexa_fluid
          elemFaceConn(jj) = elemFaceConn(jj-1) + elemFaces(jj-1) + 1
                           ! get offset from previous element index
                           ! add face dimension of the previous face
                           ! add one to the offset for the face dimension
      end do

      do jj=1,nb_hexa_fluid
          elemFaceConn(elemFaceConn(jj)) = elemFaces(jj)
      end do

    write(*,*) "Stampo il primo elemento"
    write(*,*) elemFaceConn(1)
    write(*,*) elemFaceConn(elemFaceConn(1))
    write(*,*) elemFaceConn(elemFaceConn(1)+1 :  elemFaceConn(1) + elemFaceConn(elemFaceConn(1)) )
    write(*,*) "Stampo il secondo elemento"
    write(*,*) elemFaceConn(2)
    write(*,*) elemFaceConn(elemFaceConn(2)-1)
    write(*,*) elemFaceConn(elemFaceConn(2))
    write(*,*) elemFaceConn(elemFaceConn(2)+1)
write(*,*) "CHECK"
write(*,*) "CHECK"
write(*,*) "CHECK"
write(*,*) "CHECK"


      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/owner'))
      open(40,file=gridfile)

      do jj=1,21
          read(40,'(A)') inline
      enddo

      ! read the file

      do jj=1,nb_face_fluid
          read(40,'(A)') inline
          read(inline,*) intVal
          intVal = intVal + 1       ! from C++ to Fortran
          elemIter(intVal) = elemIter(intVal) +1                       ! shift for the conn vector
          elemFaceConn(elemFaceConn(intVal) + elemIter(intVal) ) = jj  ! already Fortran
          ! shift to current element, skip the face number, 
      end do

      close(40)

      ! now read the neighbour elements
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/neighbour'))
      open(40,file=gridfile)

      do jj=1,19
          read(40,'(A)') inline
      enddo

      read(40,'(A)') inline
      read(inline,*) neigVal

      read(40,'(A)') inline     ! skip parenthesis

      ! read the file
      ! access the element and sum one to the face dimensions

      do jj=1,neigVal
          read(40,'(A)') inline
          read(inline,*) intVal
          intVal = intVal + 1       ! from C++ to Fortran
          elemIter(intVal) = elemIter(intVal) +1        ! shift for the conn vector
          elemFaceConn(elemFaceConn(intVal) + elemIter(intVal)) = jj
          ! shift to current element, skip the face number
      end do

      close(40)


      deallocate(elemIter)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      return

      end subroutine READ_MESH_FLUID_CONN_ELEMENTS
