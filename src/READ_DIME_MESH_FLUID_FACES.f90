      subroutine READ_DIME_MESH_FLUID_FACES(&
                           aeroPath, nb_face_fluid, faceFluid, nb_faces_conn_fluid)

      implicit none


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE HEAD
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      character*150 :: gridfile, inline
      character*90 :: aeroPath


      integer*4 :: jj,kk, nb_face_fluid, nb_faces_conn_fluid
      integer*4, dimension(nb_face_fluid) :: faceFluid

      integer*4       :: ierror, intVal, val             ! this checks if the input is valid

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE BODY
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      nb_faces_conn_fluid = nb_face_fluid + nb_face_fluid
  
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/faces'))
      open(40,file=gridfile)

      do jj=1,20
          read(40,'(A)') inline
      enddo
      ierror = 1
      do jj=1,nb_face_fluid

          read(40,'(A)') inline
          ! devo prendere la lunghezza fino alla parentesi aperta
          ! potrei avere due cifre
          val= INDEX(inline, "(")-1
          read(inline(1:val),*, iostat=ierror) intVal

          if ( ierror .ne. 0 ) then

              read(40,'(A)') inline
              read(inline(1:2),*) intVal
              write(*,*) inline, intVal

              faceFluid(jj) = intVal
              do kk = 1,(intVal+3)
                  read(40,'(A)') inline
              end do
              nb_faces_conn_fluid = nb_faces_conn_fluid + faceFluid(jj)

          else

              faceFluid(jj) = intVal
              nb_faces_conn_fluid = nb_faces_conn_fluid + faceFluid(jj)
          end if

      end do

      close(40)


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      return

      end subroutine READ_DIME_MESH_FLUID_FACES
