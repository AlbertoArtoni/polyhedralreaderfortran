subroutine EXPORT_VTK_POLY( dimPoints, dimConnFace, dimConnElem, &
                            points, elemFaceConn, faceConnFluid, &
                            filename)

    implicit none
    integer*4 :: dimConnFace, dimConnElem, dimPoints
    integer*4 , dimension(0:dimConnFace) :: faceConnFluid
    integer*4 , dimension(0:dimConnElem) :: elemFaceConn
    real*8 , dimension(dimPoints,3) :: points
    character(len=*), intent(in) :: filename

    integer*4 :: VTK_file_unit
    integer*4 :: nelem, nsize
    integer*4 :: ii, jj, kk, nF, nV, nTot, faceIndex, facePointer

    write(*,*) "Exporting to vtk..."

    ! @todo creare l'exporter per il vettore di poliedri, che è quello che ti serve
    !       per debuggare la parte fluida

    ! dump points

    ! @ todo new VTK
    ! strategia VTK nuovo:
    ! CELLS datatype
    ! OFFSETS
    ! CONNECTIVITY

    open(newunit=VTK_file_unit, action='WRITE', file=filename, &
                          form='FORMATTED', status='replace')

    write(VTK_file_unit,'(A)')'# vtk DataFile Version 3.0'
    write(VTK_file_unit,'(A)')'VTKFile'
    write(VTK_file_unit,'(A)')'ASCII'
    write(VTK_file_unit,'(A)')
    write(VTK_file_unit,'(A)')'DATASET UNSTRUCTURED_GRID'

    write(VTK_file_unit,'(A7,I12,A7)')'POINTS ',dimPoints,' double';
                POINT_LOOP: do jj=1,dimPoints
                    write(VTK_file_unit,'(3E16.8,1X)') points(jj,1:3)
                end do POINT_LOOP
    write(VTK_file_unit,'(A)')


    ! @ old VTK:

    nelem = elemFaceConn(0)
    nsize = 0

    do jj = 1,nelem

        nF = elemFaceConn(elemFaceConn(jj))

        nTot = 0

        do kk=1,nF ! loop over the faces. Sum up all the vertices
            faceIndex   = elemFaceConn(elemFaceConn(jj) + kk )
            facePointer = faceConnFluid(faceIndex)
            nTot = nTot + faceConnFluid(facePointer)
        end do

        nTot = nTot + nF  +1          
        nsize = nTot + nsize
    end do


    write(VTK_file_unit,'(A7,I12,I12)')'CELLS ',nelem, nsize+nelem


    do jj = 1,nelem

        nF = elemFaceConn(elemFaceConn(jj))

        nTot = 0

        do kk=1,nF ! loop over the faces. Sum up all the vertices
            faceIndex   = elemFaceConn(elemFaceConn(jj) + kk )
            facePointer = faceConnFluid(faceIndex)
            nTot = nTot + faceConnFluid(facePointer)
        end do

        nTot = nTot + nF + 1            ! devo considerare anche se stesso

        write(VTK_file_unit,'(I0)',advance='no') nTot
        write(VTK_file_unit,'(A)',advance='no') ' '
        write(VTK_file_unit,'(I0)',advance='no') nF
        write(VTK_file_unit,'(A)',advance='no') ' '

        do kk=1,nF ! loop over the faces. Dump all the connectivity
            faceIndex   = elemFaceConn(elemFaceConn(jj) + kk )
            facePointer = faceConnFluid(faceIndex)
            nV = faceConnFluid(facePointer)
            write(VTK_file_unit,'(I0)', advance='no') nV
            write(VTK_file_unit,'(A)',advance='no') ' '

            do ii=1,nV
                write(VTK_file_unit,'(I0)', advance='no') faceConnFluid(facePointer+ii)
                write(VTK_file_unit,'(A)',advance='no') ' '
            end do

        end do
        ! newline
        write(VTK_file_unit,'(A)')
    end do

    write(VTK_file_unit,'(A11,I12)')'CELL_TYPES ',nelem

    do jj = 1,nelem
        write(VTK_file_unit,'(I12)') 42     ! assume evryone to be a polyhedron
    end do

    close(unit=VTK_file_unit)

end subroutine









