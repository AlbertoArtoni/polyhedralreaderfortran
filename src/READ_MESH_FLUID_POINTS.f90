      subroutine READ_MESH_FLUID_POINTS(aeroPath, nb_nodes_fluid, points_fluid)


      implicit none

      character*90 :: aeroPath
      character*80 :: inline
      character*100 :: gridfile

      integer*4 :: nb_nodes_fluid
      integer*4 :: jj

      real*8    :: xx,yy,zz
      real*8, dimension(nb_nodes_fluid,3) :: points_fluid

      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/points'))

      !> Open mesh file
      open(40,file=gridfile)
      do jj=1,20
          read(40,'(A)') inline
      enddo
      
      ! a more consistent implementation has 3 vectors for x,y,z

      do jj=1,nb_nodes_fluid
          read(40,'(A)') inline
          read(inline(2:len_trim(inline)-1),*) xx,yy,zz
          points_fluid(jj,1) = xx
          points_fluid(jj,2) = yy
          points_fluid(jj,3) = zz
      end do


      close(40)

      end subroutine
