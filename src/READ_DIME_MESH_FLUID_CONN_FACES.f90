      subroutine READ_DIME_MESH_FLUID_CONN_FACES(&
                      aeroPath, nb_face_fluid, faceFluid, nb_faces_conn_fluid, &
                           faceConnFluid)

      implicit none


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE HEAD
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      character*150 :: gridfile, inline
      character*90 :: aeroPath

      integer*4, intent(in) :: nb_face_fluid
      integer*4 :: jj,kk, nb_faces_conn_fluid, iter
      integer*4, dimension(nb_face_fluid) :: faceFluid
      integer*4, dimension(0:nb_faces_conn_fluid) :: faceConnFluid

      integer*4       :: ierror, intVal, val             ! this checks if the input is valid
      integer*4       :: valIn, valOut, valTmp, valOld   ! iteratori sulla stringa

!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!> SUBROUTINE BODY
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      
      faceConnFluid(0) = nb_face_fluid
      faceConnFluid(1) = nb_face_fluid + 1

      do jj=2,nb_face_fluid
          faceConnFluid(jj) = faceConnFluid(jj-1) + faceFluid(jj-1) + 1
          ! +1 needed for the face size
      end do    

      iter = nb_face_fluid+1
  
      gridfile=trim(adjustl(trim(aeroPath)//'constant/polyMesh/faces'))
      open(40,file=gridfile)

      do jj=1,20
          read(40,'(A)') inline
      enddo


      write(*,*) "starting loop"

      do jj=1,nb_face_fluid

   ! I am also storing the number of caes in this vector
   ! this is a redundant information, but avoids computations
          faceConnFluid(iter) =  faceFluid(jj)
          iter = iter+1

          read(40,'(A)') inline
          ! devo prendere la lunghezza fino alla parentesi aperta
          ! potrei avere due cifre
          valIn = INDEX(inline, "(")+1
          valOut= INDEX(inline, ")")-1

          if ( valOut .eq. -1 ) then

              read(40,'(A)') inline     ! read face dimension if gt then 11
              read(40,'(A)') inline     ! skip parenthesis
              
              do kk = 1,faceFluid(jj)
                  read(40,'(A)') inline     
                  read(inline,*) intVal
                  faceConnFluid(iter) =  intVal
                  iter = iter+1
              end do
              read(40,'(A)') inline     ! skip parenthesis
              read(40,'(A)') inline     ! skip blank space

          else
              valOld = valIn
              do kk =1,faceFluid(jj)-1
                  valTmp = INDEX(inline(valOld:valOut), " ")

                  read(inline(valOld:valTmp+valOld),*) intVal
                  faceConnFluid(iter) =  intVal
                  iter = iter+1
                  valOld = valTmp+valOld
              end do

              read(inline(valOld:valOut),*) intVal

              faceConnFluid(iter) =  intVal
              iter = iter+1

          end if
          
      end do

      close(40)


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      return

      end subroutine READ_DIME_MESH_FLUID_CONN_FACES
